package com.example;

import java.util.Scanner;

public class Calculator {
    int firstNumber;
    int secondNumber;
    int result;
    public void input() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Enter a number: ");
        firstNumber = scanner.nextInt();
        System.out.print("Enter another number: ");
        secondNumber = scanner.nextInt();
    }
    public void additon(){
        try {
            result =firstNumber + secondNumber;
            System.out.println("The result of the addition is: " + result);
        }
        catch (Exception exception) {
            System.out.println(exception.getMessage());
        }
    }
    public void subtraction() {
        try {
            result = firstNumber - secondNumber;
            System.out.println("The result of the subtract is: " + result);
        }catch (Exception exception){
            System.out.println(exception.getMessage());
        }
    }

    public void division() {
        try {
            result = firstNumber / secondNumber;
            System.out.println("The result of the division is: " + result);
        }catch (Exception exception){
            System.out.println(exception.getMessage());
        }
    }

}
